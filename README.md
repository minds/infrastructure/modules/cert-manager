# cert-manager

| Node Group | Deployment | Environments          | Namespace |
| ---------- | ---------- | --------------------- | --------- |
| stateless  | Argo CD    | Sandbox, Production   | cert-manager   |

## Description

Used for managing certificate creation and rotation for applications in Kubernetes.

## Deploy

This module is deployed using Argo CD. Push changes to master, and then perform a "Sync" on the application.

Applications:
  * [Sandbox](https://gitlab.com/minds/infrastructure/minds-argocd-applications/-/blob/master/apps/oci/sandbox/cert-manager.yaml)
  * [Production](https://gitlab.com/minds/infrastructure/minds-argocd-applications/-/blob/master/apps/oci/production/cert-manager.yaml)